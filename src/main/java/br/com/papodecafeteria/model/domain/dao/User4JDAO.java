package br.com.papodecafeteria.model.domain.dao;

import java.io.FileInputStream;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import br.com.papodecafeteria.model.domain.User4J;

public class User4JDAO implements Serializable{
	private static final long serialVersionUID = 1L;
	private static final Logger l = Logger.getLogger(User4JDAO.class.getName());
	
	protected static Properties loadProperties(String pReference){
		Properties properties = null;
		try {
			FileInputStream fileInputStream = new FileInputStream(pReference);
			properties = new Properties();
			properties.load(fileInputStream);
			fileInputStream = null;
		} catch (Exception exc) {
			l.log(Level.SEVERE, exc.getMessage(), exc);
		}
		return properties;
	}

	private static Connection getConnection(){  
	    Connection conn = null;
	    try{  
	    	ClassLoader loader = Thread.currentThread().getContextClassLoader();
	    	Properties properties = new Properties();
	    	properties.load(loader.getResourceAsStream("connection.property"));
	        Class.forName(properties.getProperty("dbclass"));	        
	        String dburl = properties.getProperty("dbjdbctype") + "://" + properties.getProperty("dbaddress") + "/" + 
        					properties.getProperty("dbinstance") + "?useSSL=" + properties.getProperty("dbusessl") + 
        					"&useTimezone=" + properties.getProperty("dbusetimezone") +
        					"&serverTimezone=" + properties.getProperty("dbservertimezone"); 
	        
	        conn = DriverManager.getConnection(dburl, properties.getProperty("userdb"), properties.getProperty("userdbpasswd"));
	        properties = null;
	    } catch(Exception exc){
	    	l.log(Level.SEVERE, exc.getMessage(), exc);
	    }  
	    return conn;  
	}
	
	public static User4J insert(User4J pUser){  
	    int status = 0;  
	    
	    try{
	        PreparedStatement ps = getConnection().prepareStatement(  
	        	"insert into user(name,password,email,sex,country) values(?,?,?,?,?);", Statement.RETURN_GENERATED_KEYS);  
	        
	        ps.setString(1,pUser.getName());  
	        ps.setString(2,pUser.getPassword());  
	        ps.setString(3,pUser.getEmail());  
	        ps.setString(4,pUser.getSex());  
	        ps.setString(5,pUser.getCountry());
	        
	        status = ps.executeUpdate();
	        if(status > 0){
		        ResultSet pResultSet = ps.getGeneratedKeys();
	            if (pResultSet.next())
	            	status = pResultSet.getInt(1);
	        }
	    } catch(Exception exc){
	    	l.log(Level.SEVERE, exc.getMessage(), exc);
	    }   
	    pUser.setKey(status);
	    return pUser;  
	}
	
	public static int update(User4J pUser){  
	    int status = 0;  
	    try{
	        PreparedStatement ps = getConnection().prepareStatement(  
	        	"update user set name=?,email=?,sex=?,country=? where id=?;");
	        
	        ps.setString(1,pUser.getName());
	        ps.setString(2,pUser.getEmail());  
	        ps.setString(3,pUser.getSex());  
	        ps.setString(4,pUser.getCountry());  
	        ps.setInt(5,pUser.getKey());  
	        
	        status = ps.executeUpdate();  
	    } catch(Exception exc){
	    	l.log(Level.SEVERE, exc.getMessage(), exc);
	    }   
	    return status;  
	} 
	
	public static int delete(int pKey){  
	    int status = 0;  
	    try{
	        PreparedStatement ps = getConnection().prepareStatement("delete from user where id=?;");
	        
	        ps.setInt(1, pKey);  
	        status = ps.executeUpdate();  
	    } catch(Exception exc){
	    	l.log(Level.SEVERE, exc.getMessage(), exc);
	    }  
	    return status;  
	}
	
	public static List<User4J> getAllRecords(){  
	    List<User4J> lstUser = new ArrayList<User4J>();  
	      
	    try{
	        PreparedStatement ps = getConnection().prepareStatement("select id, name, email, sex, country from user;");  
	        ResultSet rs = ps.executeQuery(); 
	        
	        while(rs.next()){ 
	        	User4J user = new User4J();
	        	user.setKey(rs.getInt("id"));
	        	user.setName(rs.getString("name"));
	        	user.setEmail(rs.getString("email"));
	        	user.setSex(rs.getString("sex"));
	        	user.setCountry(rs.getString("country"));
	        	lstUser.add(user);
	        }  
	    } catch(Exception exc){
	    	l.log(Level.SEVERE, exc.getMessage(), exc);
	    }  
	    return lstUser;  
	}

	public static User4J getRecordById(int pKey){  
	    User4J pUser = null;  
	    
	    try{
	        PreparedStatement ps = getConnection().prepareStatement(
	        	"select id, name, email, sex, country from user where id=?;");  
	        ps.setInt(1, pKey);  
	        ResultSet rs = ps.executeQuery();  
	        
	        while(rs.next()){
	        	pUser = new User4J();
	        	pUser.setKey(rs.getInt("id"));
	        	pUser.setName(rs.getString("name"));
	        	pUser.setEmail(rs.getString("email"));
	        	pUser.setSex(rs.getString("sex"));
	        	pUser.setCountry(rs.getString("country"));
	        }  
	    } catch(Exception exc){
	    	l.log(Level.SEVERE, exc.getMessage(), exc);
	    }   
	    return pUser;  
	}

}
