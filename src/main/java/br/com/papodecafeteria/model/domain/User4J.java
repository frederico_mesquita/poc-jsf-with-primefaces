package br.com.papodecafeteria.model.domain;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;

import br.com.papodecafeteria.model.domain.vldt.Vldt;

public class User4J implements Serializable{
	private static final long serialVersionUID = 1L;
	private static final Logger l = Logger.getLogger(User4J.class.getName());
	
	public User4J(){
		super();
	}
	
	private User4J(int pKey, String pName, String pPassword, String pEmail, String pSex, String pCountry){
		try{
			setKey(pKey);
			setName(pName);
			setPassword(pPassword);
			setEmail(pEmail);
			setSex(pSex);
			setCountry(pCountry);
		} catch (Exception e) {
			l.log(Level.SEVERE, e.getMessage(), e);
		}
	}
	
	public static User4J getUser4JInstance(int pKey, String pName, String pPassword, String pEmail, String pSex, String pCountry) throws Exception{
		User4J pUser4J= null;
		try{
			pUser4J = new User4J(pKey, pName, pPassword, pEmail, pSex, pCountry);
			Vldt.validate(pUser4J);
		}catch (Exception e) {
			l.log(Level.SEVERE, e.getMessage());
			throw new Exception(e.getMessage());
		}
		return pUser4J;
	}
	
	@NotNull @PositiveOrZero
	private int key;
	
	@NotNull @Size(min=10, max=100)
	private String name;
	
	@NotNull @Size(min=5, max=10)
	private String password;
	
	@NotNull @Size(min=10, max=100) @Email
	private String email;
	
	@NotNull @Size(min=4, max=6)
	private String sex;
	
	@NotNull @Size(min=4, max=20)
	private String country;

	public int getKey() {
		return key;
	}

	public void setKey(int key) {
		this.key = key;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}
}
