package br.com.papodecafeteria.model.domain.vldt;

import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;

public class Vldt {
	private static final Logger l = Logger.getLogger(Vldt.class.getName());

	public static void validate(Object pObjToVldt) throws Exception{
		String cExceptions = "";
		try{
			Validator vldtr = Validation.buildDefaultValidatorFactory().getValidator();
			Set<ConstraintViolation<Object>> constraintViolations = vldtr.validate( pObjToVldt );
			if(constraintViolations.size() > 0){
				cExceptions = pObjToVldt.getClass().getName() + " exceptions = {";
				for(ConstraintViolation<Object> cons : constraintViolations)
					cExceptions += "[" + cons.getPropertyPath() + ":" + cons.getMessage() + "], ";
				cExceptions = cExceptions.substring(0, cExceptions.length() - 2).concat("}");
				
				throw new Exception(cExceptions);
			}
		} catch (Exception exc) {
			l.log(Level.SEVERE, exc.getMessage());
			throw new Exception(exc.getMessage());
		}
	}

	public static void validateEdition(Object pObjToVldt) throws Exception{
		String cExceptions = "";
		try{
			boolean hasMoreThanPasswd = false;
			Validator vldtr = Validation.buildDefaultValidatorFactory().getValidator();
			Set<ConstraintViolation<Object>> constraintViolations = vldtr.validate( pObjToVldt );
			if(constraintViolations.size() > 0){
				cExceptions = pObjToVldt.getClass().getName() + " exceptions = {";
				for(ConstraintViolation<Object> cons : constraintViolations)
					if(!cons.getPropertyPath().toString().equals("password")){
						cExceptions += "[" + cons.getPropertyPath() + ":" + cons.getMessage() + "], ";
						hasMoreThanPasswd = true;
					}
				if(hasMoreThanPasswd){
					cExceptions = cExceptions.substring(0, cExceptions.length() - 2).concat("}");
					throw new Exception(cExceptions);
				}
			}
		} catch (Exception exc) {
			l.log(Level.SEVERE, exc.getMessage());
			throw new Exception(exc.getMessage());
		}
	}
}
