package br.com.papodecafeteria.controller;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

import br.com.papodecafeteria.model.domain.User4J;

@ManagedBean(name = "addUserNavigationController")
@RequestScoped
public class AddUserNavigationController implements Serializable {
	private static final long serialVersionUID = 1L;
	private static final Logger l = Logger.getLogger(AddUserNavigationController.class.getName());
	
	private int key;
	private String name;
	private String password;
	private String email;
	private String sex;
	private String country;
	
	@ManagedProperty(value="#{param.pageIdNav}")
	private String pageId;
	
	public String navigateTo(){
		String page = "";
		try{
			switch (getPageId()) {
			case "addUser":
				insertUser4J();
				page = "getAllUsers?faces-redirect=true";
				break;
			default:
				page = "index?faces-redirect=true";
				break;
			}
		} catch (Exception e) {
			l.log(Level.SEVERE, e.getMessage(), e);
			
			FacesContext.getCurrentInstance().addMessage(
                    null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    e.getMessage(), "ERROR!"));
		}
		return page;
	}
	
	private void insertUser4J() throws Exception{
		try{
			User4JController.insert(
				User4J.getUser4JInstance(
					0, 
					getName(), 
					getPassword(), 
					getEmail(), 
					getSex(), 
					getCountry()
				)
			);
		}catch (Exception e) {
			l.log(Level.SEVERE, e.getMessage());
			throw new Exception(e.getMessage());
		}
	}

	public int getKey() {
		return key;
	}

	public void setKey(int key) {
		this.key = key;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getPageId() {
		return pageId;
	}

	public void setPageId(String pageId) {
		this.pageId = pageId;
	}
}
