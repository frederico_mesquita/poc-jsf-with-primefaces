package br.com.papodecafeteria.controller;

import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import br.com.papodecafeteria.model.domain.User4J;
import br.com.papodecafeteria.model.domain.dao.User4JDAO;

public class User4JController implements Serializable{
	private static final long serialVersionUID = 1L;
	private static final Logger l = Logger.getLogger(User4JController.class.getName());

	public static User4J getUser4JById(int pKey){
		User4J pUser4J = null;
		try{
			pUser4J = User4JDAO.getRecordById(pKey);
		}catch (Exception e) {
			l.log(Level.SEVERE, e.getMessage(), e);
		}
		return pUser4J;
	}
	
	public static List<User4J> getAll(){
		List<User4J> lstUser4J = null;
		try{
			lstUser4J = User4JDAO.getAllRecords();
		}catch (Exception e) {
			l.log(Level.SEVERE, e.getMessage(), e);
		}
		return lstUser4J;
	}
	
	public static User4J insert(User4J pUser4J){
		User4J pUser4JReturn = null;
		try{
			pUser4JReturn = User4JDAO.insert(pUser4J);
		}catch (Exception e) {
			l.log(Level.SEVERE, e.getMessage(), e);
		}
		return pUser4JReturn;
	}
	
	public static int update(User4J pUser4J){
		int status = 0;
		try{
			status = User4JDAO.update(pUser4J);
		}catch (Exception e) {
			l.log(Level.SEVERE, e.getMessage(), e);
		}
		return status;
	}

	public static int deleteUserr4J(int pKey){
		int status = 0;
		try{
			status = User4JDAO.delete(pKey);
		}catch (Exception e) {
			l.log(Level.SEVERE, e.getMessage(), e);
		}
		return status;
	}
}
