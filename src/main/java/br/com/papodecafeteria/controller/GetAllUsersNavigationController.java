package br.com.papodecafeteria.controller;

import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;

import br.com.papodecafeteria.model.domain.User4J;

@ManagedBean(name = "getAllUsersNavigationController")
@RequestScoped
public class GetAllUsersNavigationController implements Serializable {
	private static final long serialVersionUID = 1L;
	private static final Logger l = Logger.getLogger(GetAllUsersNavigationController.class.getName());
	
	private List<User4J> lstUser4J;
	
	@ManagedProperty(value="#{param.pageIdNav}")
	private String pageId;
	
	@ManagedProperty(value="#{param.pKey}")
	private String key;
	@ManagedProperty(value="#{param.pName}")
	private String name;
	@ManagedProperty(value="#{param.pEmail}")
	private String email;
	@ManagedProperty(value="#{param.pSex}")
	private String sex;
	@ManagedProperty(value="#{param.pCountry}")
	private String country;
	
	public String navigateTo(){
		String page = "";
		try{
			switch (getPageId()) {
			case "deleteUser":
				User4JController.deleteUserr4J(Integer.parseInt(key));
				page = "getAllUsers?faces-redirect=true";
				break;
			case "updateUser":
				page = "editUser";
				break;
			case "addUser":
				page = "addUser";
				break;
			default:
				page = "index?faces-redirect=true";
				break;
			}
		} catch (Exception e) {
			l.log(Level.SEVERE, e.getMessage(), e);
		}
		return page;
	}

	public String getPageId() {
		return pageId;
	}

	public void setPageId(String pageId) {
		this.pageId = pageId;
	}

	public List<User4J> getLstUser4J() {
		setLstUser4J(User4JController.getAll());
		return lstUser4J;
	}

	private void setLstUser4J(List<User4J> lstUser4J) {
		this.lstUser4J = lstUser4J;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}
	
}
