package br.com.papodecafeteria.controller;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;

@ManagedBean(name = "navigationController")
@RequestScoped
public class NavigationController implements Serializable{
	private static final long serialVersionUID = 1L;
	private static final Logger l = Logger.getLogger(NavigationController.class.getName());
	
	@ManagedProperty(value="#{param.pageIdNav}")
	private String pageId;

	public String navigateTo(){
		String page = "";
		try{
			switch (getPageId()) {
			case "addUser":
				page = "addUser?faces-redirect=true";
				break;
			case "getAllUsers":
				page = "getAllUsers?faces-redirect=true";
				break;
			default:
				page = "index?faces-redirect=true";
				break;
			}
		} catch (Exception e) {
			l.log(Level.SEVERE, e.getMessage(), e);
		}
		return page;
	}

	public String getPageId() {
		return pageId;
	}

	public void setPageId(String pageId) {
		this.pageId = pageId;
	}


}
