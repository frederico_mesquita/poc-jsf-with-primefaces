package br.com.papodecafeteria.controller;

import java.io.Serializable;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

import br.com.papodecafeteria.model.domain.User4J;
import br.com.papodecafeteria.model.domain.vldt.Vldt;

@ManagedBean(name = "editUserNavigationController")
@RequestScoped
public class EditUserNavigationController implements Serializable{
	private static final long serialVersionUID = 1L;
	private static final Logger l = Logger.getLogger(EditUserNavigationController.class.getName());
	
	private int    userKey;
	private String name;
	private String email;
	private String sex;
	private String country;

	@ManagedProperty(value="#{param.pageIdNav}")
	private String pageId;
	
	@ManagedProperty(value="#{param.pKey}")
	private String key;
	
	public String navigateTo(){
		String page = "";
		try{
			switch (getPageId()) {
			case "editUser":
				getKeyFromContext();
				updateUser4JContext();
				page = "editUser";
				break;
			case "updateUser":
				User4JController.update(getUser4J4Update());
				page = "getAllUsers?faces-redirect=true";
				break;
			case "cancel":
				page = "getAllUsers?faces-redirect=true";
				break;
			default:
				page = "index?faces-redirect=true";
				break;
			}
		} catch (Exception e) {
			l.log(Level.SEVERE, e.getMessage(), e);
			
			FacesContext.getCurrentInstance().addMessage(
                    null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    e.getMessage(), "ERROR!"));
		}
		return page;
	}
	
	private void updateUser4JContext(){
		try{
			User4J pUser4J = User4JController.getUser4JById(Integer.parseInt(getKey()));
			setName(pUser4J.getName());
			setEmail(pUser4J.getEmail());
			setSex(pUser4J.getSex());
			setCountry(pUser4J.getCountry());
			pUser4J = null;
		}catch (Exception e) {
			l.log(Level.SEVERE, e.getMessage(), e);
		}
	}
	
	private void getKeyFromContext(){
		try{
			Map<String,String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
			setKey(params.get("pKey"));
			setUserKey(Integer.parseInt(params.get("pKey")));
		}catch (Exception e) {
			l.log(Level.SEVERE, e.getMessage(), e);
		}
		return;
	}
	
	private User4J getUser4J4Update() throws Exception{
		User4J pUser4J = null;
		try{
			pUser4J = new User4J();
			pUser4J.setKey(getUserKey());
			pUser4J.setName(getName());
			pUser4J.setEmail(getEmail());
			pUser4J.setSex(getSex());
			pUser4J.setCountry(getCountry());
			Vldt.validateEdition(pUser4J);
		}catch (Exception e) {
			l.log(Level.SEVERE, e.getMessage());
			throw new Exception(e.getMessage());
		}
		return pUser4J;
	}

	public String getPageId() {
		return pageId;
	}

	public void setPageId(String pageId) {
		this.pageId = pageId;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public int getUserKey() {
		return userKey;
	}

	public void setUserKey(int userKey) {
		this.userKey = userKey;
	}
	
}
