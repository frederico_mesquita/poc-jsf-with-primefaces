package br.com.papodecafeteria.controller;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;

@ManagedBean(name = "deleteUserNavigationController")
@RequestScoped
public class DeleteUserNavigationController implements Serializable {
	private static final long serialVersionUID = 1L;
	private static final Logger l = Logger.getLogger(DeleteUserNavigationController.class.getName());
	
	@ManagedProperty(value="#{param.pageIdNav}")
	private String pageId;
	
	@ManagedProperty(value="#{param.pKey}")
	private String key;
	
	public String navigateTo(){
		String page = "";
		try{
			switch (getPageId()) {
			case "deleteUser":
				User4JController.deleteUserr4J(Integer.parseInt(key));
				page = "getAllUsers?faces-redirect=true";
				break;
			default:
				page = "index?faces-redirect=true";
				break;
			}
		} catch (Exception e) {
			l.log(Level.SEVERE, e.getMessage(), e);
		}
		return page;
	}

	public String getPageId() {
		return pageId;
	}

	public void setPageId(String pageId) {
		this.pageId = pageId;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}
}
